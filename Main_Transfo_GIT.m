%% entete
% nom et prenom
% date de la creation 
clear all; clc;
% clear all; %vider le command window
% clc ;
% close all ; % fermer toutes les figures
%%
%% declaration des variables
x = [20e-3,55e-3,0.023,0.053,800,1.018e-05,3.96e-06] ;
%%
 a = x(1) ; %largeur noyau lateral
 b = x(2) ; % hauteur fenetre
 c = x(3) ; % largeur fenetre
 d = x(4) ;
 n1 = x(5) ;
 S1fil = x(6) ;
 S2fil = x(7) ;
%% declaration des parametres
% parametres

% constants (unit, meaning in french)
V2 = 24;                % (V) tension secondaire
V1 = 230;               % (V) tension primaire
f = 50;                 % (Hz) frequence
fp2 = 0.8;              % facteur de puissance secondaire
I2 = 8;                 % (A) courant secondaire
Text = 40;              % (C) temperature exterieur
q = 1;                  % (W/kg) qualite de tole
kr = 0.5;               % Coefficient de remplissage des encoches
h = 10;                 % (W/m2/K) coefficient de convection de l'air
lambda = 0.15;          % (W/m/K) coefficient de conduction de l'isolant
e_isol = 1e-3;          % (m) epaisseur de l'isolant
mvfer = 7800;           % (kg/m3) masse volumique du fer
mvcuivre = 8800;        % (kg/m3) masse volumique du cuivre
rhocuivre = 1.72e-8;    % (ohm.m) resistivite du cuivre
alphacuivre = 3.8e-3;   % (1/K) variation de la resistivite du cuivre

%%
variables = x ;
%% declaration du modele de conception phase 3 : Introduction de la notion d'un point de départ valide

% Dans la phase 3, ils doivent revoir le point initial et partir d'un point 
% initial plus viable en initialisant Tcu_0 et DV2_0
Tcuivre_0 = 1 ;
DV2_0 = 1 ;
parametres = [V2,V1,f,fp2,I2,Text,q,kr,h,lambda,e_isol,mvfer,mvcuivre,rhocuivre,alphacuivre, Tcuivre_0, DV2_0]; %vecteurs
Sorties = Modele_conception(variables,parametres) ;


