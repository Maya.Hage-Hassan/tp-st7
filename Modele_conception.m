function [Sorties] = Modele_conception(variables,parametres)
%MODELE_CONCEPTION Summary of this function goes here
%   Detailed explanation goes here

 a = variables(1) ; %largeur noyau lateral
 b = variables(2) ; % hauteur fenetre
 c = variables(3) ; % largeur fenetre
 d = variables(4) ;
 n1 = variables(5) ;
 S1fil = variables(6) ;
 S2fil = variables(7) ;
 %
V2 = parametres (1);                % (V) tension secondaire
V1 = parametres (2);               % (V) tension primaire
f = parametres (3);                 % (Hz) frequence
fp2 = parametres (4);              % facteur de puissance secondaire
I2 = parametres (5);                 % (A) courant secondaire
Text = parametres (6);              % (C) temperature exterieur
q = parametres (7);                  % (W/kg) qualite de tole
kr = parametres (8);               % Coefficient de remplissage des encoches
h = parametres (9);                 % (W/m2/K) coefficient de convection de l'air
lambda = parametres (10);          % (W/m/K) coefficient de conduction de l'isolant
e_isol = parametres(11);          % (m) epaisseur de l'isolant
mvfer = parametres (12);           % (kg/m3) masse volumique du fer
mvcuivre = parametres (13);        % (kg/m3) masse volumique du cuivre
rhocuivre = parametres (14);    % (ohm.m) resistivite du cuivre
alphacuivre = parametres (15);   % (1/K) variation de la resistivite du cuivre
Tcuivre_0 = parametres (16);
DV2_0  = parametres (17);

%% Ensemble 1
    
    muo = 4*pi*1e-7;
    Bm = 1/4*V1*sqrt(2)/(n1*a*d*pi*f);
    l1spire = 2*(d+2*a)+pi*c/2;
    Mfer = 4*a*d*(2*a+b+c)*mvfer;
    Pfer = q*Mfer*f/50*(Bm/1)^2;
    Rfercuivre = e_isol/(lambda*b*(4*a+2*d));
    Sferair = 4*a*(4*a+b+2*c)+2*d*(6*a+b+2*c);
    Rferair = 1/(h*Sferair);
    Scuivreair = b*(4*a+2*pi*c);
    Rcuivreair = 1/(h*Scuivreair);
    l2spire = 2*(d+2*a)+pi*3*c/2;
%%      
Tcuivre = Tcuivre_0;
DV2 = DV2_0 ;         
            % systeme d'equations (initialisation)
            
            n2 = n1*(V2+DV2)/V1;
            r2 = rhocuivre*(1+alphacuivre*Tcuivre)*n2*l2spire/S2fil;
            X2 = 1/3*muo*n2^2*c*(4*a+2*d+pi*c)/b*2*pi*f;
            r1 = rhocuivre*(1+alphacuivre*Tcuivre)*n1*l1spire/S1fil;
            R2 = r2+(n2/n1)^2*r1;
            DV2 = (R2*fp2+X2*sin(acos(fp2)))*I2;
            Pj = R2*I2^2;
            Tcuivre = Text+Rcuivreair*(Rferair*Pfer+(Rferair+Rfercuivre)...
                *Pj)/(Rcuivreair+Rferair+Rfercuivre);

 %%  Systeme implicite
 % initialisation de la methode newton-Raphson 
 xo = [r2, X2, n2, DV2, Pj, Tcuivre, r1,R2] ;


 %%  Systeme implicite
 % initialisation de la methode newton-Raphson 
%  xo = [0, 0, 0, 0, 0, 0, 0,0] ;
 N_max = 100 ;
 epsilon = 1e-8;
 err = 1 ;
 N_iter = 1 ;
 X(:,N_iter) = xo ;
 Xn = xo' ; 
 
 while (err>epsilon)&&(N_iter<N_max)
 
     [FN, JN] = sys_equa (X(:,N_iter)) ;
     
     DeltaX = -JN\(FN') ;
 
     Xn = Xn + DeltaX ; % Vn a literation n+1
     
     err = norm(DeltaX)/(norm(Xn)) ;
     
     N_iter = N_iter + 1 ;
     
     X(:,N_iter) = Xn ; %mise a jour de Vn
     
 end
%  N_max
N_iter ;
 x = X(:,N_iter) ;
            r2 = x(1);
            X2 = x(2);
            n2 = x(3);
            DV2 = x(4);
            Pj = x(5);
            Tcuivre = x(6);
            r1 = x(7);
            R2 = x(8);          
% % if (err<epsilon && Tcuivre>0 && DV2 >0)
% %     residu = norm(sys_equa(x));
% % else
% %     residu = 1e6 ;
% % end

if (Tcuivre<0 || DV2 <0)
      residu = 1e6 ;
else
  residu = norm(sys_equa(x));
end
%%            
%  Ensemble 2
    
    Tfer = Text+Rferair*(Rcuivreair*Pj+(Rcuivreair+Rfercuivre)*Pfer)/(Rcuivreair+Rferair+Rfercuivre);
    Lmu=muo*(1/(2.12E-4+((1-2.12E-4)*(Bm/1)^(2*7.358))/((Bm/1)^(2*7.358)+1.18E+6)))*n1^2*a*d/(2*a+b+c);
    Mcuivre = mvcuivre*(n1*S1fil*l1spire+n2*S2fil*l2spire);
    ren = V2*I2*fp2/(V2*I2*fp2+Pfer+Pj);
    P1 = Pfer+Pj+V2*I2*fp2;
    Q1 = V1^2/(Lmu*2*pi*f)+X2*I2^2+V2*I2*sin(acos(fp2));
    I1 = sqrt(P1^2+Q1^2)/V1;
    I10 = sqrt((Pfer/V1)^2+(V1/(Lmu*2*pi*f))^2);
    f1=2*n1*S1fil/(kr*b*c);
    f2=2*n2*S2fil/(kr*b*c);
    M_tot = Mcuivre+Mfer;

    %% vecteur sortie
    
 Sorties =  [M_tot, Tcuivre, Tfer, DV2/V2, I10/I1, ren, f1, f2, Pj, Pfer,residu] ;
 
 %% systeme implicite a rajouter phase 2
 
     function [F,J] = sys_equa(x)
        
        r2=x(1);
        X2=x(2);
        n2=x(3);
        DV2=x(4);
        Pj=x(5);
        Tcuivre=x(6);
        r1=x(7);
        R2=x(8);
        
        % residu du systeme implicite de 8 equations
        
        F(1)=r2-rhocuivre*(1+alphacuivre*Tcuivre)*n2*l2spire/S2fil;
        F(2)=X2-1/3*muo*n2^2*c*(4*a+2*d+pi*c)/b*2*pi*f;
        F(3)=n2-n1*(V2+DV2)/V1;
        F(4)=DV2-(R2*fp2+X2*sin(acos(fp2)))*I2;
        F(5)=Pj-R2*I2^2;
        F(6)=Tcuivre-Text-Rcuivreair*(Rferair*Pfer+(Rferair+Rfercuivre)*Pj)/(Rcuivreair+Rferair+Rfercuivre);
        F(7)=r1-rhocuivre*(1+alphacuivre*Tcuivre)*n1*l1spire/S1fil;
        F(8)=R2-r2-(n2/n1)^2*r1;
        
        if nargout>1
            
            % Jacobien du systeme implicite de 8 equations
            
            J = [1, 0, -rhocuivre*(1+alphacuivre*Tcuivre)*l2spire/S2fil, 0, 0, -rhocuivre*alphacuivre*n2*l2spire/S2fil, 0, 0;
                0, 1, -4/3*muo*n2*c*(4*a+2*d+pi*c)*pi*f/b, 0, 0, 0, 0, 0;
                0, 0, 1, -n1/V1, 0, 0, 0, 0;
                0, -sin(acos(fp2))*I2, 0, 1, 0, 0, 0, -fp2*I2;
                0, 0, 0, 0, 1, 0, 0, -I2^2;
                0, 0, 0, 0, -Rcuivreair*(Rferair+Rfercuivre)/(Rcuivreair+Rferair+Rfercuivre), 1, 0, 0;
                0, 0, 0, 0, 0, -rhocuivre*alphacuivre*n1*l1spire/S1fil, 1, 0;
                -1, 0, -2*n2/n1^2*r1, 0, 0, 0, -n2^2/n1^2, 1];
            
        end
        
    end

end

